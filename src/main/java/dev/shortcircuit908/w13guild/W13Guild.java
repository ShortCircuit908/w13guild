package dev.shortcircuit908.w13guild;

import com.google.common.reflect.TypeToken;
import dev.shortcircuit908.w13guild.commands.CommandGuild;
import dev.shortcircuit908.w13guild.guild.Guild;
import dev.shortcircuit908.w13guild.guild.GuildJoinRequest;
import dev.shortcircuit908.w13guild.guild.GuildManager;
import org.bukkit.command.PluginCommand;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.command.Command;
import org.bukkit.plugin.java.annotation.command.Commands;
import org.bukkit.plugin.java.annotation.permission.ChildPermission;
import org.bukkit.plugin.java.annotation.permission.Permission;
import org.bukkit.plugin.java.annotation.permission.Permissions;
import org.bukkit.plugin.java.annotation.plugin.ApiVersion;
import org.bukkit.plugin.java.annotation.plugin.LogPrefix;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.Set;

@Plugin(name = "w13guild", version = "1.0-SNAPSHOT")
@ApiVersion(ApiVersion.Target.v1_15)
@Author("ShortCircuit908")
@LogPrefix("W13G")
@Commands(
		{
				@Command(
						name = "guild join",
						desc = "Request to join a guild, or accept an invitation",
						permission = "guild.command.join",
						usage = "/<command> <guild>"
				),
				@Command(
						name = "guild invite",
						desc = "Invite someone to your guild, or accept a request to join",
						permission = "guild.command.invite",
						usage = "/<command> [guild] <player>"
				),
				@Command(
						name = "guild kick",
						desc = "Forcibly remove someone from your guild",
						permission = "guild.command.kick",
						usage = "/<command> [guild] <player>"
				),
				@Command(
						name = "guild leave",
						desc = "Leave your guild",
						permission = "guild.command.leave",
						usage = "/<command> [guild]"
				),
				@Command(
						name = "guild create",
						desc = "Create a new guild",
						permission = "guild.command.create",
						usage = "/<command> <guild>"
				),
				@Command(
						name = "guild disband",
						desc = "Disband your guild",
						permission = "guild.command.create",
						usage = "/<command> [guild]"
				),
				@Command(
						name = "guild request",
						aliases = "guild requests",
						desc = "Approve or deny new guilds",
						permission = "guild.admin",
						usage = "/<command> [<guild> [accept|deny]]"
				),
				@Command(
						name = "guild role",
						desc = "Create or remove a role within a guild",
						permission = "guild.command.create",
						usage = "/<command> <create|remove> [guild] <role>\n/<command> <grant|revoke> [guild] " +
								"<player> <role>"
				),
				@Command(
						name = "guild",
						desc = "Dummy command",
						usage = "/guild join, /guild invite, /guild kick, /guild leave, /guild disband, /guild " +
								"request, /guild role",
						permission = "guild.command"
				)
		}
)
@Permissions(
		{
				@Permission(
						name = "guild.admin",
						desc = "Manage all guilds and guild requests",
						defaultValue = PermissionDefault.OP
				),
				@Permission(
						name = "guild.command",
						desc = "Base permission for guild commands",
						defaultValue = PermissionDefault.TRUE,
						children = {
								@ChildPermission(
										name = "guild.command.join"
								),
								@ChildPermission(
										name = "guild.command.invite"
								),
								@ChildPermission(
										name = "guild.command.kick"
								),
								@ChildPermission(
										name = "guild.command.leave"
								),
								@ChildPermission(
										name = "guild.command.create"
								)
						}
				),
				@Permission(
						name = "guild.create.bypass-cooldown",
						desc = "Bypass the guild creation cooldown",
						defaultValue = PermissionDefault.OP
				),
				@Permission(
						name = "guild.join.bypass-cooldown",
						desc = "Bypass the guild membership cooldown",
						defaultValue = PermissionDefault.OP
				)
		}
)
public class W13Guild extends JavaPlugin {
	private final GuildManager guild_manager = new GuildManager();
	private final GuildConfig config = new GuildConfig(this);
	
	@Override
	public void onLoad() {
		
	}
	
	@Override
	public void onEnable() {
		reloadConfig();
		loadGuilds();
		loadGuildRequests();
		
		PluginCommand guild_command = getCommand("guild");
		guild_command.setExecutor(new CommandGuild(this, guild_command));
	}
	
	@Override
	public void onDisable() {
		saveGuilds();
		saveGuildRequests();
	}
	
	public GuildManager getGuildManager() {
		return guild_manager;
	}
	
	public GuildConfig getGuildConfig() {
		return config;
	}
	
	public void loadGuilds() {
		try {
			Set<Guild> guilds = loadGuilds("guilds.json");
			Set<Guild> guild_requests = loadGuilds("guild_requests.json");
			if (guilds != null) {
				guild_manager.setGuilds(guilds);
			}
			if (guild_requests != null) {
				guild_manager.setGuildRequests(guild_requests);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
			getLogger().severe("Error loading guilds from file");
		}
	}
	
	public void saveGuilds() {
		try {
			saveGuilds(guild_manager.getGuilds(), "guilds.json");
			saveGuilds(guild_manager.getGuildRequests(), "guild_requests.json");
		}
		catch (IOException e) {
			e.printStackTrace();
			getLogger().severe("Error writing guilds to file");
		}
	}
	
	public void loadGuildRequests() {
		try {
			Set<GuildJoinRequest> join_requests = loadGuildRequests("guild_join_requests.json");
			if (join_requests != null) {
				guild_manager.setGuildJoinRequests(join_requests);
			}
			Set<GuildJoinRequest> invite_requests = loadGuildRequests("guild_invite_requests.json");
			if (invite_requests != null) {
				guild_manager.setGuildInviteRequests(invite_requests);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
			getLogger().severe("Error loading guild join requests from file");
		}
	}
	
	public void saveGuildRequests() {
		try {
			saveGuildRequests(guild_manager.getGuildJoinRequests(), "guild_join_requests.json");
			saveGuildRequests(guild_manager.getGuildInviteRequests(), "guild_invite_requests.json");
		}
		catch (IOException e) {
			e.printStackTrace();
			getLogger().severe("Error writing guild join requests to file");
		}
	}
	
	private Set<GuildJoinRequest> loadGuildRequests(String file) throws IOException {
		File f = getDataFolder().toPath().resolve(file).toFile();
		if (!f.exists()) {
			return null;
		}
		Type type = new TypeToken<Set<GuildJoinRequest>>() {
		}.getType();
		try (Reader reader = new FileReader(f)) {
			return Util.GSON.fromJson(reader, type);
		}
	}
	
	public void saveGuildRequests(Set<GuildJoinRequest> requests, String file) throws IOException {
		File f = getDataFolder().toPath().resolve(file).toFile();
		if (!f.exists()) {
			f.getParentFile().mkdirs();
			f.createNewFile();
		}
		try (Writer writer = new FileWriter(f)) {
			Util.GSON.toJson(requests, writer);
			writer.flush();
		}
	}
	
	private Set<Guild> loadGuilds(String file) throws IOException {
		File f = getDataFolder().toPath().resolve(file).toFile();
		if (!f.exists()) {
			return null;
		}
		Type type = new TypeToken<Set<Guild>>() {
		}.getType();
		try (Reader reader = new FileReader(f)) {
			return Util.GSON.fromJson(reader, type);
		}
	}
	
	public void saveGuilds(Set<Guild> guilds, String file) throws IOException {
		File f = getDataFolder().toPath().resolve(file).toFile();
		if (!f.exists()) {
			f.getParentFile().mkdirs();
			f.createNewFile();
		}
		try (Writer writer = new FileWriter(f)) {
			Util.GSON.toJson(guilds, writer);
			writer.flush();
		}
	}
}
