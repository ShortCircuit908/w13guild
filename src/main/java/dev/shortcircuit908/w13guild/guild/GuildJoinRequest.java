package dev.shortcircuit908.w13guild.guild;

import java.util.Objects;
import java.util.UUID;

public class GuildJoinRequest {
	private final String target_guild;
	private final UUID target_player;
	private final int hash_code;
	
	public GuildJoinRequest(String target_guild, UUID target_player) {
		this.target_guild = target_guild;
		this.target_player = target_player;
		this.hash_code = Objects.hash(target_guild.toLowerCase(), target_player);
	}
	
	public String getTargetGuild() {
		return target_guild;
	}
	
	public UUID getTargetPlayer() {
		return target_player;
	}
	
	@Override
	public int hashCode() {
		return hash_code;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof GuildJoinRequest &&
				this.target_guild.equalsIgnoreCase(((GuildJoinRequest) o).target_guild) &&
				this.target_player.equals(((GuildJoinRequest) o).target_player);
	}
}
