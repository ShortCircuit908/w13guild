package dev.shortcircuit908.w13guild.guild;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Guild {
	private final String name;
	private UUID owner;
	private final Set<String> roles = new HashSet<>();
	private final Set<UUID> members = new HashSet<>();
	private final Map<UUID, Set<String>> member_roles = new HashMap<>();
	private transient int hash_code;
	private transient boolean hash_is_zero;
	
	public Guild(String name, UUID owner) {
		this.name = name;
		this.owner = owner;
	}
	
	public Guild(String name, UUID owner, Collection<String> roles, Collection<UUID> members) {
		this.name = name;
		this.owner = owner;
		if (roles != null) {
			this.roles.addAll(roles);
		}
		if (members != null) {
			this.members.addAll(members);
		}
	}
	
	public String getName() {
		return name;
	}
	
	public UUID getOwner() {
		return owner;
	}
	
	public void setOwner(UUID owner) {
		this.owner = owner;
	}
	
	public Set<String> getRoles() {
		return roles;
	}
	
	public Set<UUID> getMembers() {
		return members;
	}
	
	public Map<UUID, Set<String>> getMemberRoles() {
		return member_roles;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Guild
				&& this.name.equalsIgnoreCase(((Guild) o).name);
	}
	
	@Override
	public int hashCode() {
		if (hash_is_zero || hash_code != 0) {
			return hash_code;
		}
		hash_code = name.toLowerCase().hashCode();
		hash_is_zero = hash_code == 0;
		return hash_code;
	}
}
