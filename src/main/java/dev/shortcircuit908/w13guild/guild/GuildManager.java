package dev.shortcircuit908.w13guild.guild;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class GuildManager {
	private final Set<Guild> guilds = new HashSet<>();
	private final Set<Guild> requests = new HashSet<>();
	private final Set<GuildJoinRequest> guild_join_requests = new HashSet<>();
	private final Set<GuildJoinRequest> guild_invite_requests = new HashSet<>();
	
	public boolean addGuild(Guild guild) {
		return guilds.add(guild);
	}
	
	public Set<Guild> getGuilds() {
		return Collections.unmodifiableSet(guilds);
	}
	
	public Optional<Guild> getGuild(String name) {
		return getGuild(guilds, name);
	}
	
	public Set<Guild> getGuildsWithMember(UUID member_id) {
		return getGuildWithMember(guilds, member_id);
	}
	
	public boolean addGuildRequest(Guild request) {
		if (getGuild(request.getName()).isPresent()) {
			return false;
		}
		return requests.add(request);
	}
	
	public Set<Guild> getGuildRequests() {
		return Collections.unmodifiableSet(requests);
	}
	
	public Optional<Guild> getGuildRequest(String name) {
		return getGuild(requests, name);
	}
	
	public Set<Guild> getGuildRequestsWithMember(UUID member_id) {
		return getGuildWithMember(requests, member_id);
	}
	
	public Set<GuildJoinRequest> getGuildJoinRequests() {
		return Collections.unmodifiableSet(guild_join_requests);
	}
	
	public Set<GuildJoinRequest> getGuildInviteRequests() {
		return Collections.unmodifiableSet(guild_invite_requests);
	}
	
	public boolean inviteToGuild(GuildJoinRequest request) {
		if (guild_join_requests.remove(request)) {
			// Accept a join request
			getGuild(request.getTargetGuild()).ifPresent(value -> value.getMembers().add(request.getTargetPlayer()));
			return true;
		}
		guild_invite_requests.add(request);
		return false;
	}
	
	public boolean requestToJoinGuild(GuildJoinRequest request) {
		if (guild_invite_requests.remove(request)) {
			// Accept an invite request
			getGuild(request.getTargetGuild()).ifPresent(value -> value.getMembers().add(request.getTargetPlayer()));
			return true;
		}
		guild_join_requests.add(request);
		return false;
	}
	
	private static <T extends Guild> Optional<T> getGuild(Collection<T> guilds, String name) {
		return guilds.stream().filter(guild -> guild.getName().equalsIgnoreCase(name)).findFirst();
	}
	
	private static <T extends Guild> Set<T> getGuildWithMember(Collection<T> guilds, UUID member_id) {
		return guilds.stream()
				.filter(guild -> guild.getMembers()
						.stream()
						.anyMatch(member -> member.equals(member_id)))
				.collect(Collectors.toSet());
	}
	
	public void setGuilds(Collection<Guild> guilds) {
		this.guilds.clear();
		this.guilds.addAll(guilds);
	}
	
	public void setGuildRequests(Collection<Guild> guilds) {
		this.requests.clear();
		this.requests.addAll(guilds);
	}
	
	public void setGuildJoinRequests(Collection<GuildJoinRequest> requests) {
		this.guild_join_requests.clear();
		this.guild_join_requests.addAll(requests);
	}
	
	public void setGuildInviteRequests(Collection<GuildJoinRequest> requests) {
		this.guild_invite_requests.clear();
		this.guild_invite_requests.addAll(requests);
	}
}
