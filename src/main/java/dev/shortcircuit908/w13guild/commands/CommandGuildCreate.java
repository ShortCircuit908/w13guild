package dev.shortcircuit908.w13guild.commands;

import dev.shortcircuit908.w13guild.guild.Guild;
import dev.shortcircuit908.w13guild.W13Guild;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;

public class CommandGuildCreate implements CommandExecutor {
	private final W13Guild plugin;
	
	protected CommandGuildCreate(W13Guild plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1) {
			return false;
		}
		
		Guild new_guild = new Guild(args[0], (sender instanceof OfflinePlayer ?
				((OfflinePlayer) sender).getUniqueId() : null));
		
		if (!plugin.getGuildConfig().getGuildCreationCooldown().isZero() &&
				!sender.hasPermission("guild.create.bypass-cooldown")) {
			
		}
		boolean result;
		boolean is_request = plugin.getGuildConfig().guildsRequireApproval() && !sender.hasPermission("guild.admin");
		if (is_request) {
			result = plugin.getGuildManager().addGuildRequest(new_guild);
		}
		else {
			result = plugin.getGuildManager().addGuild(new_guild);
		}
		if (!result) {
			sender.sendMessage(ChatColor.RED + "A guild with that name already exists");
		}
		else {
			if (is_request) {
				sender.sendMessage(ChatColor.GOLD + "Your guild application has been submitted for approval");
			}
			else {
				sender.sendMessage(ChatColor.GOLD + "Your guild has been registered");
			}
			sender.sendMessage(String.format(
					"%2$sInvite members with %3$s/guild invite %1$s <player>",
					new_guild.getName(),
					ChatColor.GOLD,
					ChatColor.AQUA
			));
		}
		return true;
	}
}
