package dev.shortcircuit908.w13guild.commands;

import dev.shortcircuit908.w13guild.Util;
import dev.shortcircuit908.w13guild.W13Guild;
import dev.shortcircuit908.w13guild.guild.Guild;
import dev.shortcircuit908.w13guild.guild.GuildJoinRequest;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class CommandGuildJoin implements CommandExecutor {
	private final W13Guild plugin;
	
	public CommandGuildJoin(W13Guild plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof OfflinePlayer)) {
			sender.sendMessage(ChatColor.RED + "This command is player-only");
			return true;
		}
		if (args.length != 1) {
			return false;
		}
		UUID sender_id = ((OfflinePlayer) sender).getUniqueId();
		Guild target_guild = plugin.getGuildManager().getGuild(args[0]).orElse(null);
		if (target_guild == null) {
			sender.sendMessage(String.format(
					"%2$sNo guild named %3$s%1$s",
					args[0],
					ChatColor.RED,
					ChatColor.AQUA
			));
			return true;
		}
		if (target_guild.getMembers().contains(sender_id)) {
			sender.sendMessage(String.format(
					"%2$sYou are already a member of %3$s%1$s",
					target_guild.getName(),
					ChatColor.RED,
					ChatColor.AQUA
			));
			return true;
		}
		GuildJoinRequest request = new GuildJoinRequest(target_guild.getName(), sender_id);
		boolean result = plugin.getGuildManager().requestToJoinGuild(request);
		if (result) {
			sender.sendMessage(String.format(
					"%2$sAccepted an invitation to join %3$s%1$s",
					target_guild.getName(),
					ChatColor.GOLD,
					ChatColor.AQUA
			));
		}
		else {
			sender.sendMessage(String.format(
					"%2$sSent a request to join %3$s%1$s",
					target_guild.getName(),
					ChatColor.GOLD,
					ChatColor.AQUA
			));
		}
		return true;
	}
}