package dev.shortcircuit908.w13guild.commands;

import com.google.common.base.Joiner;
import dev.shortcircuit908.w13guild.MapUtil;
import dev.shortcircuit908.w13guild.W13Guild;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;

import java.util.Arrays;
import java.util.Map;

public class CommandGuild implements CommandExecutor {
	private final W13Guild plugin;
	private final Map<String, CommandExecutor> subcommands;
	
	public CommandGuild(W13Guild plugin, Command self_command) {
		this.plugin = plugin;
		this.subcommands = MapUtil.ofEntries(
				MapUtil.entry("guild create", new CommandGuildCreate(plugin)),
				MapUtil.entry("guild invite", new CommandGuildInvite(plugin)),
				MapUtil.entry("guild join", new CommandGuildJoin(plugin))
		);
		self_command.setUsage(Joiner.on(", ")
				.join(subcommands.keySet().stream().map(command -> "/" + command).iterator()));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String[] compare = new String[args.length + 1];
		System.arraycopy(args, 0, compare, 1, args.length);
		compare[0] = "guild";
		subcommand_loop:
		for (Map.Entry<String, CommandExecutor> subcommand : subcommands.entrySet()) {
			String[] parts = subcommand.getKey().split(" ");
			if (compare.length < parts.length) {
				continue;
			}
			for (int i = 0; i < parts.length; i++) {
				if (!compare[i].equalsIgnoreCase(parts[i])) {
					continue subcommand_loop;
				}
			}
			String[] new_args = new String[compare.length - parts.length];
			System.arraycopy(args, parts.length - 1, new_args, 0, new_args.length);
			boolean result = subcommand.getValue().onCommand(sender, command, subcommand.getKey(), new_args);
			if (!result) {
				Command subcommand_cmd = plugin.getCommand(subcommand.getKey());
				if (subcommand_cmd == null) {
					return false;
				}
				for (String line : subcommand_cmd.getUsage().replace("<command>", subcommand.getKey()).split("\n")) {
					sender.sendMessage(line);
				}
			}
			return true;
		}
		return false;
	}
}
