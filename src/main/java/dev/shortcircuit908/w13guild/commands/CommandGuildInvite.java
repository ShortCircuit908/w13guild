package dev.shortcircuit908.w13guild.commands;

import dev.shortcircuit908.w13guild.Util;
import dev.shortcircuit908.w13guild.W13Guild;
import dev.shortcircuit908.w13guild.guild.Guild;
import dev.shortcircuit908.w13guild.guild.GuildJoinRequest;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class CommandGuildInvite implements CommandExecutor {
	private final W13Guild plugin;
	
	public CommandGuildInvite(W13Guild plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1 || args.length > 2) {
			return false;
		}
		String target_player_raw = args[args.length - 1];
		UUID sender_id = sender instanceof OfflinePlayer ? ((OfflinePlayer) sender).getUniqueId() : null;
		Guild target_guild;
		if (args.length > 1) {
			target_guild = plugin.getGuildManager().getGuild(args[0]).orElse(null);
			if (target_guild == null) {
				sender.sendMessage(String.format(
						"%2$sNo guild named %3$s%1$s",
						args[0],
						ChatColor.RED,
						ChatColor.AQUA
				));
				return true;
			}
		}
		else {
			Set<Guild> guilds = plugin.getGuildManager().getGuildsWithMember(sender_id);
			if (guilds.size() > 1) {
				sender.sendMessage(ChatColor.RED + "You are a member of multiple guilds. Please specify which guild " +
						"to use");
				return true;
			}
			if (guilds.isEmpty()) {
				sender.sendMessage(ChatColor.RED + "You are not a member of a guild");
				return true;
			}
			target_guild = guilds.stream().findFirst().get();
		}
		if (!Objects.equals(target_guild.getOwner(), sender_id) && !sender.hasPermission("guild.admin")) {
			sender.sendMessage(ChatColor.RED + "Only the guild's owner can send invites");
			return true;
		}
		OfflinePlayer target_player = Bukkit.getServer().getPlayer(target_player_raw);
		if (target_player == null) {
			target_player = Util.getMostRecentOfflinePlayer(target_player_raw).orElse(null);
		}
		if (target_player == null) {
			sender.sendMessage(String.format(
					"%2$sUnknown player %3$s%1$s",
					target_player_raw,
					ChatColor.RED,
					ChatColor.AQUA
			));
			return true;
		}
		
		if (target_guild.getMembers().contains(target_player.getUniqueId())) {
			sender.sendMessage(String.format(
					"%4$s%1$s%3$s is already a member of %4$s%2$s",
					target_player.getName(),
					target_guild.getName(),
					ChatColor.RED,
					ChatColor.AQUA
			));
			return true;
		}
		
		GuildJoinRequest request = new GuildJoinRequest(target_guild.getName(), target_player.getUniqueId());
		boolean result = plugin.getGuildManager().inviteToGuild(request);
		if (result) {
			sender.sendMessage(String.format(
					"%3$sAccepted %4$s%1$s%3$s request to join %4$s%2$s",
					Util.possessive(target_player.getName()),
					target_guild.getName(),
					ChatColor.GOLD,
					ChatColor.AQUA
			));
		}
		else {
			sender.sendMessage(String.format(
					"%3$sInvited %4$s%1$s%3$s to join %4$s%2$s",
					target_player.getName(),
					target_guild.getName(),
					ChatColor.GOLD,
					ChatColor.AQUA
			));
		}
		return true;
	}
}
