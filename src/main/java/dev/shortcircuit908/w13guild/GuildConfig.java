package dev.shortcircuit908.w13guild;

import org.bukkit.plugin.Plugin;

import java.time.Duration;

public class GuildConfig {
	private final Plugin plugin;
	
	public GuildConfig(Plugin plugin) {
		this.plugin = plugin;
	}
	
	public boolean guildsRequireApproval() {
		return plugin.getConfig().getBoolean("guilds-require-approval");
	}
	
	public Duration getGuildCreationCooldown() {
		return readDuration("guild-create-cooldown");
	}
	
	public Duration getGuildJoinCooldown() {
		return readDuration("guild-join-cooldown");
	}
	
	private Duration readDuration(String path) {
		String raw = plugin.getConfig().getString(path);
		if (raw == null) {
			return Duration.ZERO;
		}
		try {
			return DurationUtil.fromString(raw);
		}
		catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return Duration.ZERO;
	}
}
