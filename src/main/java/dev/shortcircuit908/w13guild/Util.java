package dev.shortcircuit908.w13guild;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

public class Util {
	public static final Gson GSON = new GsonBuilder()
			.serializeNulls()
			.setPrettyPrinting()
			.disableHtmlEscaping()
			.create();
	
	public static final Pattern UUID_PATTERN = Pattern.compile(
			"([0-9a-f]{8})-?([0-9a-f]{4})-?([0-9a-f]{4})-?([0-9a-f]{4})-?([0-9a-f]{12})",
			Pattern.CASE_INSENSITIVE
	);
	
	public static String uuidToString(UUID uuid) {
		// Remove dashes between UUID sections
		return UUID_PATTERN.matcher(uuid.toString().toLowerCase()).replaceAll("$1$2$3$4$5");
	}
	
	public static UUID stringToUuid(String string) {
		// Add dashes between UUID sections
		return UUID.fromString(UUID_PATTERN.matcher(string).replaceAll("$1-$2-$3-$4-$5"));
	}
	
	public static Optional<OfflinePlayer> getMostRecentOfflinePlayer(String username) {
		// Find all offline players with a matching username and select the one that joined the server most recently
		return Arrays.stream(Bukkit.getServer().getOfflinePlayers())
				.filter((p) -> Objects.equals(p.getName(), username))
				.max((p1, p2) -> Comparator.comparingLong(OfflinePlayer::getLastPlayed).compare(p1, p2));
	}
	
	public static boolean isOp(UUID uuid) {
		// This is necessary because I'm unsure if the entries in ops.json are included in Server::getOfflinePlayer()
		for (OfflinePlayer operator : Bukkit.getServer().getOperators()) {
			if (uuid.equals(operator.getUniqueId())) {
				return true;
			}
		}
		return false;
	}
	
	public static String possessive(String str){
		return str + (str.endsWith("s") ? "'" : "'s");
	}
}
